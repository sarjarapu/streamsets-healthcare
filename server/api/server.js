(function(){

  'use strict';
  
  var express = require('express');
  var cors = require('cors')
  var bodyParser = require('body-parser');
  var fs = require('fs');
  var appRoutes = require('./common/app.routes');
  var logger = require('./common/custom.logger')('server.js');
  var port = process.env.PORT || 8080;
  var app = express();
  
  
  app.use(cors());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  
  appRoutes.addRoutes(app);
  
  app.use(function(req, res, next) {
    if (res.result !== undefined){
      res.json(res.result);
    }
    else {
      next();
    }
  });
  
  app.use(function(req, res) {
    logger.warn(`no service listening at requested endPoint: [${req.method}] ${req.originalUrl} `);
    res.status(404).send({url: req.originalUrl + ' not found'})
  });
  
  app.listen(port);
  
  logger.info(`health-care RESTful api server is up. Listening on: ${port}`);
  
  }())
  
  