(function(){

  'use strict';
  var winston = require('winston');
  var moment = require('moment');
  var path = require('path');
  var config = require('../conf/config');
  
  
  class CustomLogger extends winston.Logger {
    constructor(name) {
      var transports = getLogTransports(name);
      super({
        level : 'debug',
        handleExceptions: false,
        transports: transports,
        exitOnError: false
      });
      this.loggerName = name;
    }
  
    getLoggerName() {
      return this.loggerName;
    }
  
  }
  
  const getLogTransports = (name) => {
    var transports = [];
    var ct = getConsoleTransport(name);
    if (ct != null) {
      transports.push(ct);
    }
    var ft = getFileTransport(name);
    if (ft != null) {
      transports.push(ft);
    }
    return transports;
  }
  
  const getConsoleTransport = (name) => {
    if (process.env.NODE_ENV !== 'production') {
      return new (winston.transports.Console)({
        timestamp() {
          return moment().format('YYYY-MM-DD HH:mm:ss.SSSS');
        },
        json: false,
        formatter: function(options) {
          // Return string will be passed to logger.
          return `[${options.level.toUpperCase()}] ${options.timestamp()} ${name} - ${(options.message ? options.message : '')}`;
        }
      })
    }
    return null;
  }
  
  const getFileTransport = (name) => {
    if (process.env.NODE_ENV === 'production') {
      var filename = process.env.LOG_FILE || "/tmp/trader-monk.generic.log";
      return new winston.transports.File({ filename: filename, level: 'info',
        timestamp() {
          return moment().format('YYYY-MM-DD HH:mm:ss.SSSS');
        },
        json: false,
        formatter: function(options) {
          // Return string will be passed to logger.
          return `[${options.level.toUpperCase()}] ${options.timestamp()} ${name} - ${(options.message ? options.message : '')}`;
        }
      });
    }
    else {
      return null;
    }
  }
  
  module.exports = function(name) {
    return new CustomLogger(name)
  };
  
  
  }());