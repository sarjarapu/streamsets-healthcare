(function(){

'use strict';
var hbRoutes = require('../heartbeat/routes');
var nRoutes = require('../notifier/routes');

class AppRoutes {
	addRoutes(app) {
		hbRoutes.addRoutes(app);
		nRoutes.addRoutes(app);
	}
}
module.exports = new AppRoutes();

}());