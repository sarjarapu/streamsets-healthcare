(function(){

'use strict';
var moment = require('moment');
var uuid = require('uuid/v4');
var logger = require('./custom.logger')('common.base.controller.js');
var utils = require('./utils');

class BaseController {
	constructor(service, endpoint) {
		this.service = service;
		this.endpoint = endpoint;
	}

	wrapTimeAndSendIt(req, res, query, getPromise, next) {		
		var uri = this.getUri(req);
		var service = this.service;
		var endpoint = this.endpoint;
		var startDate = utils.getCurrentDateTime();
		var result = {
			metadata : {
				endpoint: {
					service: service, endpoint: endpoint, uri : uri
				},
				request: {
					id: uuid().replace(/-/g,''),
					operation: { type: "query", query: query }
				},
				statistics : {
					initiatedAt: startDate.toISOString()
				}
			}
		};
		
		var promise = getPromise();
		promise.then(data => {
			var endDate = moment(utils.getCurrentDateTime());
			result.metadata.statistics.timeInMillis = endDate.diff(startDate, 'ms');
			result.data = data;
			res.result = result;
			if (next == undefined) {
				res.json(result);
			}
			else {
				res.result = result;
				next();
			}
		})
		.catch(error => {
			var message = JSON.stringify(error);
			if (message == "{}") {
				message = `name: ${error.name}, message: ${error.message}, stack: ${error.stack.replace(/\n/g, " ")}`;
			}
			logger.error(`error while processing wrapTimeAndSendIt; details: ${message} `);
			result.error = error;
			res.json(result);
		});
	}

	getUri(req) {
		return req.protocol + "://" + req.get('host') + req.originalUrl;
	}
}

module.exports = BaseController;

}());