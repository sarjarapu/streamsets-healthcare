(function(){

'use strict';
var Promise = require('bluebird');
var mongodb = Promise.promisifyAll(require("mongodb"));
var MongoClient = mongodb.MongoClient;
var config = require('../conf/config');

class BaseHelper {
	constructor(service, endpoint) {
		this.service = service;
		this.endpoint = endpoint;
	}

	getConnection() {
		var url = config.connectionString;
		return MongoClient
			.connectAsync(url)
			.disposer(db => db.close());
	}

	getUri(req) {
		return req.protocol + "://" + req.get('host') + req.originalUrl;
	}
}

module.exports = BaseHelper;

}());