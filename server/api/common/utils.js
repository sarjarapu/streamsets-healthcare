
const moment = require('moment');
const Promise = require('bluebird');
const config = require('../conf/config');
const mongodb = Promise.promisifyAll(require("mongodb"));
const MongoClient = mongodb.MongoClient;
const logger = require('./custom.logger')('utils.js');
const timezone = "America/New_York";


const getDate = (date) => {
	const utcOffset = 5;
	var newDate = (date ? new Date(date) : new Date()).setUTCHours(utcOffset,0,0,0)
	return new Date(newDate);
}

const getCurrentDateTime = () => {
	return new Date();
}

const getConnection = () => {
	var url = config.connectionString;
	return MongoClient
		.connectAsync(url)
		.disposer(db => db.close());
}

const getRecentAvailableDateAsync = () => {
	return Promise.using(getConnection(), 
		client => {

			var beforeDate = moment().add(-7, "days").toDate();
			var query = [
				{"$match": {"on_date": {"$gte": beforeDate}}}, 
				{"$project": {"on_date": 1, "_id": 0}}, 
				{"$sort": {"on_date": -1}}, 
				{"$limit" : 1}
			];
			return client.db('trader-monk').collectionAsync('downloader.status')
				.then(collection => collection.aggregate(query).toArray());
		}
	).then(data => {
		return data[0].on_date;
	})
	.catch(error => {
		logger.error(error);
		throw error;
	});
}

const getCurrentDateAsString = () => {
	var date = getDate(getCurrentDateTime());
	var value = moment(date).format('YYYY-MM-DD');
	// value = "2017-08-24"; // do your overrides here
	return value;
}

const getRecentAvailableDateAsStringAsync = () => {
	return getRecentAvailableDateAsync()
		.then(recentDate => {
			var date = getDate(recentDate);
			var value = moment(date).format('YYYY-MM-DD');
			return value;
		})
}

const getDateOrRecentDateAsync = (date) => {
	if (date) {
		return Promise.resolve(getDate(date));
	} 
	else {
		return getRecentAvailableDateAsync();
	}
}


const isWeekDay = () => {
  return moment.tz(timezone).isoWeekday() < 6;  // Sun = 7, Mon = 1
}

const inTradingWindow = (includePreMarket) => {
  var t = moment.tz(timezone); 
  var h = t.get('hour');

  var afterHours = includePreMarket ? 1 : 0; 
  var startHours = 9 - afterHours; 
  var endHours = 16 + afterHours; 

	if (h > startHours && h < endHours) { // between 9:00 AM - 5:00 PM
		logger.info('Trading Window: Currently in market trading hours')
    return true;
  }
	else if (h == startHours && t.get('minute') >= 30) { // only after 9:30
		logger.info('Trading Window: Currently in market trading hours')
    return true;
	}
	logger.info('Trading Window: outside of trading hours')
  return false;
}


module.exports = {
	getDate,
	getCurrentDateTime,
	getCurrentDateAsString,
	getRecentAvailableDateAsStringAsync,
	getDateOrRecentDateAsync,
	isWeekDay,
	inTradingWindow
}