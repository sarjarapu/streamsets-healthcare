(function(){

'use strict';
var controller = require('./controller');

class HeartbeatRoutes {
	addRoutes(app) {
		app.get('/hb/:id', controller.getById.bind(controller));
		app.post('/hb/', controller.create.bind(controller));
		// app.get('/hb/', controller.getAll.bind(controller));
		// app.put('/hb/:id', controller.update.bind(controller));
		// app.delete('/hb/:id', controller.deleteById.bind(controller));
	}
}
module.exports = new HeartbeatRoutes();

}());