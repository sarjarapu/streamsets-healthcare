(function(){

'use strict';

var _ = require('lodash');
var Promise = require('bluebird');
var BaseController = require('../common/base.controller');
var helper = require('./helper');

class HeartbeatController extends BaseController {
	constructor() {
		super("user", "heartbeat");
	}

	getById(req, res) {
		var id = req.params.id;
		var query = { _id: id };
		this.wrapTimeAndSendIt(req, res, query, 
			() => helper.getById(id)
		);
	}

	create(req, res) {
		var data = req.body;
		var query = { data: data }
		this.wrapTimeAndSendIt(req, res, query,  
			() => helper.create(data)
		);
	}
}

module.exports = new HeartbeatController();

}());