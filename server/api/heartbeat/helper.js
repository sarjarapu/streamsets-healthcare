(function(){

'use strict';
var _ = require('lodash');
var Promise = require('bluebird');
var mongodb = Promise.promisifyAll(require("mongodb"));
var ObjectID = mongodb.ObjectID;
var MongoClient = mongodb.MongoClient;
var config = require('../conf/config');

class HeartbeatHelper {

	getById(id) {
		return Promise.resolve({
			id: id,
			data: "what " + new Date().toString()
		})
		// return Promise.using(this.getConnection(), 
		// 	client => {
		// 		var filter = { _id: this.getIdForFind(id) };
		// 		return client.db('trader-monk').collectionAsync('users')
		// 			.then(collection => collection.findOne(filter))
		// 			.then(data => {
		// 				data.href = '/user/' + data._id;
		// 				return data;
		// 			});
		// 	}
		// );
	}


	create(fname, lname, email) {
		return Promise.resolve({
			data: "insert requested"
		})
		// var user = { fname: fname, lname: lname, email: email, watchlists: [] }
		// return Promise.using(this.getConnection(), 
		// 	client => {
		// 		return client.db('trader-monk').collectionAsync('users')
		// 			.then(collection => collection.insertOne(user))
		// 			.then(insertResult => {
		// 				user.href = '/user/' + user._id;
		// 				return Promise.resolve(user);
		// 			});
		// 	}
		// );
	}

	getIdForFind(id) {
		return (id||"").toString().startsWith("SYSTEM")
			? id 
			: new ObjectID(id)
	}

	getConnection() {
		var url = config.connectionString;
		return MongoClient
			.connectAsync(url)
			.disposer(db => db.close());
	}
}

module.exports = new HeartbeatHelper();

}());