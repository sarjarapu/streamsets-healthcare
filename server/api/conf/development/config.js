var config = {
    environment: 'development',
    logLevel: 'debug',
    connectionString: 'mongodb://localhost:27017/health-care',
    apiBaseUri: 'http://localhost:8080'
};

module.exports = config;