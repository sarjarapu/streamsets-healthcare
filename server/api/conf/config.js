var path = require("path");
var extend = require("util").inherits;

var devConfig = require("./development/config");
var prodConfig = require("./production/config");

var defaults = {
    environment: 'default'
};

var allConfigs = {
   development: Object.assign({}, defaults, devConfig),
   production:  Object.assign({}, defaults, prodConfig)
};

var environment = allConfigs.hasOwnProperty(process.env.NODE_ENV) 
    ? process.env.NODE_ENV 
    : "development";
module.exports = allConfigs[environment];

