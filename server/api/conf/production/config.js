var config = {
    environment: 'production',
    logLevel: 'info',
    connectionString: 'mongodb://apiuser:healthcare@127.0.0.1:28000/health-care?authSource=admin&replicaSet=rs0',
    apiBaseUri: 'http://localhost:8080'
};

module.exports = config;