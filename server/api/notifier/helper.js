
const Promise = require('bluebird');
const Twilio = require('twilio');
const logger = require('../common/custom.logger')('notifier.helper.js');

class NotifierHelper {
  constructor() {
    this.notifyMode = 'LIVE';
    this.toPhoneNumbers = [
      '+12014526958',
      '+17378083516'
    ]
  }

  notify(request)  {
    return this.notifyCDC(request)
      .then(_ => this.notifyUser(request))
      .catch(err => {
        logger.error("Error while sending notification to CDC/User " + JSON.stringify(request))
      })
      .bind(this);
  }

  notifyCDC(request) {
    logger.info("CDC Notification : " + JSON.stringify(request));
    return Promise.resolve(request);
  }

  notifyUser(request) {
    if (request.type == 'user') {
      return this.sendSMSToNumber(request.record).then(_ => request);
    }
    return Promise.resolve(request);
  }

  sendSMSToNumber(record) {
    var configTwilioTest = {
      accountSid: 'ACc861043edbc2408a6b5f36b06dd449f8',
      authToken: 'b44ce522748da480f821694829615ee9',
      fromPhone: '+15005550006',
      toPhone: this.toPhoneNumbers
    };

    var configTwilioLive = {
      accountSid: 'AC5ad073b032ef6c9befa21638878e458a',
      authToken: '4e643b1cecdb98636071c381bc4d353d',
      fromPhone: '+17377778889',
      toPhone: this.toPhoneNumbers
    };

    var configTwilio = this.notifyMode == 'LIVE' ? configTwilioLive : configTwilioTest;

    var client = Twilio(configTwilio.accountSid, configTwilio.authToken);
    var messageObj = {
      from: configTwilio.fromPhone,
      to: configTwilio.toPhone,
      body: `Healthcare WatchDog notification: ${record.first_name}, ${record.last_name}'s health is at risk. Location: (${record.location.lat},${record.location.lon}). Notifying nearest Emergency Medical Hospital.`
    }

    logger.info("User Notification : " + JSON.stringify(messageObj));
    // send SMS message for each user you know of 
    return client.messages.create(messageObj);
  }
}

module.exports = new NotifierHelper();
