(function(){

'use strict';

var _ = require('lodash');
var Promise = require('bluebird');
var BaseController = require('../common/base.controller');
var helper = require('./helper');
const logger = require('../common/custom.logger')('notifier.helper.js');

class NotifierController extends BaseController {
	constructor() {
		super("user", "notifier");
	}

	notify(req, res) {
		var id = req.params.id;
		var record = req.body;
		var type = req.query.type;
		var request = { record: record, type: type };

		this.wrapTimeAndSendIt(req, res, request, 
			() => helper.notify(request)
		);
	}
}

module.exports = new NotifierController();

}());