const controller = require('./controller');

class NotifierRoutes {
  addRoutes(app) {
    app.post('/notifier', controller.notify.bind(controller));
  }
}
module.exports = new NotifierRoutes();
