
# Streamsets Demo for B-Fit Healthcare

## Quick Links
1. *`Streamsets`*: http://localhost:18630/
2. *`Kibana`*: http://localhost:5601/
3. *`Elasticsearch`*: http://localhost:9200/hbdata/
4. *`Bit Bucket`*: https://bitbucket.org/sarjarapu/streamsets-healthcare.git
---

## Preparation

1. Open *Google Voice* GUI :  https://voice.google.com/messages
2. Open *Kibana* GUI :  http://localhost:5601/
3. Open *Streamsets* GUI :  http://localhost:18630/
4. Stop all the three pipelines.
5. Pipeline: *`BFit Demo - Generate Data`* Open & reset the Origin for pipeline, 
```
Pipelines / BFit Demo - Generate Data > Reset Origin
```
6. Pipeline: *`BFit Demo - Enhancements`* Open
7. Open terminal to *Clean up* the data and *restart* services
```
healthcare_app="/Users/shyamarjarapu/Code/apps/streamsets/demo/streamsets-healthcare"
cd "$healthcare_app/scripts"

sh cleanup.data.sh
sh stop.services.sh
sh start.services.sh
sh generate.data.sh
tail -f "$healthcare_app/server/api/nohup.log"

```

---

## Demo discussions

### Scenario

*`Company`*: B-Fit health care

*`Business`*: Sells fitness tracking devices to customer. These *IOT devices* gather bio metrics of the users and sends them at regular intervals to the B-Fit web servers for further analysis. The registered (basic/premium) users can view the *trend analysis* of their personal metrics online

*`Future Enhancements`*: 
The company is in rapid growth stage and wants to grow the company by adding more features to the product users. Here are some of the initiatives from the company.

1. Use Machine Learning to classify and find *`likelihood of user falling ill`*
2. *`EMR, Family & Friends`* notification services in case of fatal disease
3. Run *`location based real time analytics`*  on these metrics 
4. Work with health care insurance providers to offer *`better pricing on insurance products`* 
5. Be first to know of any *`disease outbreaks`*  / health risk patterns
6. *`Integration with Federal`*  Centers for Disease Control & Prevention (CDC)

I would like to demo how Streamsets can help B-Fit health care acheive the goal 
1. Very simply, 
2. Quickly 
3. Most importantly, with little to no coding

### Set #1

1. Start *`BFit Demo - Generate Data`* pipeline.
2. Show the *Kibana*, *Google Voice* and *Streamsets* GUI before proceeding
3. Move the *`parts-aa.csv`* files from *temp* folder to *origin*
4. Show the data is generated through *BFit Demo - Generate Data* pipeline.
5. *`Open`* BFit Demo - *`Enhancements`* pipeline, show that it's in *Stopped* State 
6. *`Origins`*, *`Destinations`* and *`Processors`* various options to work with
7. Tool can be used for *`Designing`* and *`Testing`* via Preview mode
8. *Field `Type Coverter`* and *`Jython Evaluator`* Health Risk data tranformations
9. *`Field Masker`* and *`Field Hasher`* to hide personal information for data scientists
10. Evaluate complex expressions in *`Jython Evaluator`* 

### Set #2

1. *`Start`* BFit Demo - *`Enhancements`* pipeline
2. *`Monitoring Summary`* - Metrics on Record Count, Throughput, *`Stage Processing Time`*.
3. *`Expression Evaluator`* similar metrics for *`IO & Error`*. Likewise at each stage
4. *`Errors & Configuration`* tabs for required fields and Precondition checks
5. *`Gauge sanity`* of input by running *`rules`* on sample data in motion 
6. Notice the *`output of Jython Evaluator`* is going in as (copy of) input for *`multiple stages`*
    1. *`Elasticsearch`* for in-house data processing
    2. *`HDFS`* / *`Apache Spark`* for data scientists to crunch data
    3. Notification services for *`CDC`* / *`Family n Friends`* 
7. *`Notification services`* is complex of them of all.
8. *Health Risk `Stream Selector`* helps the input *`split into multiple streams`*. Show Input/Output 
9. Likewise the *Membership Type `Stream Selector`* sends notifications to *`HTTP Client`* destination
10. *`CDC`* notification is for all. Everything else into *`Trash`*

### Set #3

1. *`Record Deduplicator`* dedupes multiple metrics from same user
2. Show *`Kibana`* dashboard and log file from *`API Service`* written in nodejs
2. Explain *`CDC`* / *`User`* notifications, Show *`Google Voice`*
3. Move the *`parts-abc.csv`* files from *temp* folder to *origin*
4. Show any new *`CDC`* / *`User`* notifications in *`Console / Phone / Google Voice`*

### Conclusion

The Streamsets Datacollector is an *`award-winning`* open source software for development of data pipelines. The Streamsets Datacollector is able attain this feet only by *`evolving day in/out`* and by *`adding support`* for integration with *`various products of BigData echo system`* such as 

```
Apache Kafka, RabbitMQ, Apache Spark / Kudu, Sqoop, 
Flume Elasticsearch, Cloudera, Hortonworks, Amazon Web Services, 
MongoDB, Oracle / SQL Server, etc.
```

to name a few. There are *`many libraries`* readily available in *`Package Manager`*. You could also *`extend the functionality`* by writing *`custom connector code`* to fit your needs.

Finally, many of my clients are still using batch processing jobs. The complain I often hear in a typical large firms is 

> Our EOD `batch processing` is taking `too much time` to process. We would like to process using streams but our `up-stream / down-stream` systems are `not ready yet`.

With the Streamsets, you do not need to worry about such limitations/hurdles. This is quite possible because, `best products` in the market, such as Streamsets, `flexible` and `powerful enough` to cater the `needs of the both worlds`. When the teams are ready, all you need to do is simply `change` the Origin/Destination `connectors` and leave every other stage in the pipeline as is.


