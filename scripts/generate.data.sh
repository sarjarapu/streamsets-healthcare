#!/bin/sh
# Data Collector UI: 
# http://localhost:18630
ss_root="/Users/shyamarjarapu/Code/apps/streamsets/demo/streamsets-healthcare"
ss_data="$ss_root/data/hb-data"
ss_scripts="$ss_root/scripts"

# clean up the data directory 
cd ~
rm -rf $ss_data/*
mkdir -p $ss_data/{origin,destination,error,temp}


# download sample set of users from mockaroo
cd $ss_data/temp
curl "https://api.mockaroo.com/api/0941e800?count=100&key=64dd7100" > "users.csv"
python $ss_scripts/populate.beats.py


# split the sample data into multiple files 
head -1 raw.csv > header.csv
cat raw.csv | sed '1d;$d' > raw_noheader.csv
split -l 100000 raw_noheader.csv "parts-"
for file in `ls parts*`
do
  cat header.csv $file > "$file.csv"
  rm "$file"
done
cp users.csv "$ss_root/docs/`date +"%Y%m%d%H%M"`-users.csv"
rm raw_noheader.csv header.csv raw.csv users.csv

cd $ss_scripts

