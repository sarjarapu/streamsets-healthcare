#!/bin/sh

healthcare_app="/Users/shyamarjarapu/Code/apps/streamsets/demo/streamsets-healthcare"
cd $healthcare_app

# stop zookeeper and kafka
kill -SIGUSR1 `ps -ef  | grep zookeeper | awk '{print $2}'`
kill -SIGUSR1 `ps -ef  | grep kafka | awk '{print $2}'`

# stop elasticsearch and kibana
brew services stop elasticsearch
brew services stop kibana

# stop streamsets application
kill -SIGUSR1 `ps -ef | grep "server.js" | grep -v " Code" | awk '{print $2}'`

# stop the nodejs API service 
kill -9 `ps -ef | grep "server.js" | grep -v " Code" | awk '{print $2}'`

rm -f "$healthcare_app/server/api/nohup.log"
touch "$healthcare_app/server/api/nohup.log"


