#!/bin/sh

# start zookeeper and kafka
kafka="/Users/shyamarjarapu/Code/apps/kafka"
healthcare_app="/Users/shyamarjarapu/Code/apps/streamsets/demo/streamsets-healthcare"
streamsets_dc="/Users/shyamarjarapu/Code/apps/streamsets/streamsets-datacollector-3.0.3.0"
kafka_topic="ssd-hbd"


# start kafka & zookeeper and start the topics
nohup $kafka/bin/zookeeper-server-start.sh $kafka/config/zookeeper.properties  > $kafka/logs/nohup.log 2>&1 &
sleep 10
nohup $kafka/bin/kafka-server-start.sh $kafka/config/server.properties  > $kafka/logs/nohup.log 2>&1 &

# start elasticsearch and kibana
brew services start elasticsearch
brew services start kibana

# start streamsets application
nohup $streamsets_dc/bin/streamsets dc  > $streamsets_dc/log/nohup.log 2>&1 &

# start the nodejs API service 
cd $healthcare_app/server/api/
nohup nodemon $healthcare_app/server/api/server.js --inspect-brk  > $healthcare_app/server/api/nohup.log 2>&1 &

# reset the pwd back to the app level
cd $healthcare_app
