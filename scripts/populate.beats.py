import random
import csv
from datetime import datetime, timedelta

cdt=datetime.now().replace(microsecond=0)
minute_factor=5
maxtime=10*24*60/minute_factor
random.seed( 3 )

def load_users_file(filename):
  users=[]
  with open(filename) as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
      users.append(row)
  return users

def write_special_cases(filename, users):
  rf_location=random.randrange(0,100)
  rf_hbpm=random.randrange(0,100)
  data=[]
  print('generating custom data for %d) %s' % (100, 'Special scenarios'))
  max_users=len(users)
  for ui in range(0, max_users):
    u = users[ui]
    print('generating data for %d) %s ' % (ui, u['email']))
    u['infected'] = False
    u['deceased'] = False
    u['body_temp'] = float(random.randrange(95,98))
    u['hbpm'] = float(random.randrange(60,150))
    u['on_date'] = (cdt - timedelta(minutes=-1*minute_factor))
    u['body_temp'] = float(u['body_temp'])
    u['hbpm'] = float(u['hbpm'])
    u['latitude'] = float(u['latitude'])
    u['longitude'] = float(u['longitude'])
    if (u['deceased'] == False):
      u['latitude'] += random.randrange(-10,10)/10000.0
      u['longitude'] += random.randrange(-10,10)/10000.0
      if (u['city'] == 'Houston'):
        u['body_temp'] += random.randrange(-20,30)/250.0
        u['hbpm'] += random.randrange(-20,30)/10.0
      else:
        u['body_temp'] += random.randrange(-10,10)/250.0
        u['hbpm'] += random.randrange(-10,10)/10.0
      if (u['body_temp'] > 108 or u['body_temp'] < 85 ):
        u['infected'] = True
        if (int(u['id'])%2 <> 0):
          u['deceased'] = True
          u['body_temp'] = 107.9
      else:
        u['infected'] = False
    u['body_temp'] = str(u['body_temp'])
    u['hbpm'] = str(u['hbpm'])
    u['latitude'] = str(u['latitude'])
    u['longitude'] = str(u['longitude'])
    x=u.copy()
    # produce some error data here 
    if(ui%rf_location == 0):
      x['latitude'] = '0.0'
      x['longitude'] = '0.0'
    if(ui%rf_hbpm == 0):
      x['hbpm'] = str(abs(float(x['hbpm']))*-1)
    # premium
    if(int(u['id'])%15 == 0):
      x['infected'] = True
    if(int(u['id'])%30 == 0):
      x['deceased'] = True
    if(int(u['id'])%75 == 0):
      x['body_temp'] = random.randrange(1080,1150)/10.0
    
    # basic
    if(int(u['id'])%16 == 0):
      x['infected'] = True
    if(int(u['id'])%33 == 0):
      x['deceased'] = True
    if(int(u['id'])%48 == 0):
      x['body_temp'] = random.randrange(1080,1150)/10.0
    data.append(x)

  custom_file=filename
  with open(custom_file, 'a') as csvfile:
    columns = ['id','first_name','last_name','email','username','gender','membership','ssn','street_address','city','state','country','latitude','longitude','on_date','body_temp','hbpm','infected','deceased']
    writer = csv.DictWriter(csvfile, fieldnames=columns)
    for d in data:
      writer.writerow(d)
  return

def write_random_cases(filename, users):
  with open('raw.csv', 'w') as csvfile:
    columns = ['id','first_name','last_name','email','username','gender','membership','ssn','street_address','city','state','country','latitude','longitude','on_date','body_temp','hbpm','infected','deceased']
    writer = csv.DictWriter(csvfile, fieldnames=columns)
    writer.writeheader()
  i=0
  rf_location=random.randrange(0,10000)*3
  rf_hbpm=random.randrange(0,10000)/3
  max_users=len(users)
  for ui in range(0, max_users):
    data=[]
    u = users[ui]
    print('generating data for %d) %s ' % (ui, u['email']))
    u['infected'] = False
    u['deceased'] = False
    u['body_temp'] = float(random.randrange(95,98))
    u['hbpm'] = float(random.randrange(60,150))
    for t in range(0, maxtime):
      i+=1
      u['on_date'] = (cdt - timedelta(minutes=t*minute_factor))
      u['body_temp'] = float(u['body_temp'])
      u['hbpm'] = float(u['hbpm'])
      u['latitude'] = float(u['latitude'])
      u['longitude'] = float(u['longitude'])
      if (u['deceased'] == False):
        u['latitude'] += random.randrange(-10,10)/10000.0
        u['longitude'] += random.randrange(-10,10)/10000.0
        if (u['city'] == 'Houston'):
          u['body_temp'] += random.randrange(-20,30)/250.0
          u['hbpm'] += random.randrange(-20,30)/10.0
        else:
          u['body_temp'] += random.randrange(-10,10)/250.0
          u['hbpm'] += random.randrange(-10,10)/10.0
        if (u['body_temp'] > 108 or u['body_temp'] < 85 ):
          u['infected'] = True
          if (int(u['id'])%2 <> 0):
            u['deceased'] = True
            u['body_temp'] = 107.9
        else:
          u['infected'] = False
      u['body_temp'] = str(u['body_temp'])
      u['hbpm'] = str(u['hbpm'])
      u['latitude'] = str(u['latitude'])
      u['longitude'] = str(u['longitude'])
      x=u.copy()

      # produce some error data here 
      if(i%rf_location == 0):
        x['latitude'] = '0.0'
        x['longitude'] = '0.0'
      if(i%rf_hbpm == 0):
        x['hbpm'] = str(float(x['hbpm'])*-1)

      data.append(x)
    with open('raw.csv', 'a') as csvfile:
      columns = ['id','first_name','last_name','email','username','gender','membership','ssn','street_address','city','state','country','latitude','longitude','on_date','body_temp','hbpm','infected','deceased']
      writer = csv.DictWriter(csvfile, fieldnames=columns)
      for d in data:
        writer.writerow(d)


users=load_users_file('users.csv')
write_special_cases('parts-abb', users)
write_random_cases('raw.csv', users)

