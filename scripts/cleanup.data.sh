#!/bin/sh

kafka="/Users/shyamarjarapu/Code/apps/kafka"
kafka_topic="ssd-hbd"

# clean up mysql data 
mysql -u bfit_user --password=P@\$\$w0rd bfit <<EOF
TRUNCATE TABLE bfit.heart_beats;
TRUNCATE TABLE bfit.users;
EOF

# clean up elasticsearch
curl -XPOST --data '{"query": { "match_all": {} }}'  -H "Content-Type: application/json" http://localhost:9200/hbdata/beat/_delete_by_query  --user "elastic:#9MK&U2I@S0HkDgc_nYI"

# clean up Kafka
# $kafka/bin/kafka-topics.sh --list --zookeeper localhost:2181
$kafka/bin/kafka-topics.sh --zookeeper localhost:2181 --delete --topic $kafka_topic
$kafka/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic $kafka_topic --config retention.ms=3600000
$kafka/bin/kafka-topics.sh --list --zookeeper localhost:2181

# clean up streamsets sample data
ss_hbdata="/Users/shyamarjarapu/Code/apps/streamsets/demo/streamsets-healthcare/data/hb-data"
mv $ss_hbdata/origin/* $ss_hbdata/temp/

echo "Clean up completed successfully"
